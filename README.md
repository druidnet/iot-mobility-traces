# Iot Mobility Traces
This repository contains tools and scripts that can be used to generate reproducible traces for IoT applications taking into account mobility.
It provides the required scripts and source codes to configure and execute these experiments as well as some auxilliary resources that helps better understanding the functionning of the tools.

It also contains traces of traces of such experiments corresponding to different combinations of parameters, for a total of multiple tens of hours and tens of millions of exchanged messages.


## Repository Structure
The repository is organized as follow.

- `src`: contains an example of source code to be executed by IoT nodes
- `scripts`: contains scripts to configure and launch the experiment, retrieve the logs, parse them and produce well formated datasets. It also contains few other helper scripts.
- `res`: contains some helper resources used to launch experiments (robots circuits files, monitoring profiles, etc.). These resources are used by scripts from the previous directory
- `exps`: contains traces from some experiments. Each folder in this directory has the following structure
    - An `exp_desc.yaml` file describing the experiment (used nodes, duration, robots circuits, application parameters, etc.)
    - A `raw` directory containing raw execution logs, both on the transmitter (tx) and receiver (rx) sides: messages sent and received, instantaneous robots location and instantaneous energy consumption of all nodes
    - An `aggr` directory containing structured csv datasets obtained by parsing and joining the data from `raw` directory.
- `demo-video.mp4`: a demo video of the testbed during a running experiment
- `fit-iot-lab-testbed.jpg`: a picture of the testbed during a running experiment
- `iot-lab-lille-deployment`: a map of the room in which are performed the experiments
- `node-properties.csv`: a csv file with information for all nodes in the testbed, including their location (x, y, z)
- `README.md`: this readme file

## Experiment Results
After running an experiment (using `exp.sh` script in `scripts` directory), obtained results are saved in `exps/EXPID/aggr/` directory.
They are composed of two structured datasets, `tx_aggr.log` for the tx side and `rx_aggr.log` for the rx side.

- Features reported for on the tx side are:
	|                 	| **timestamp**          	| **tx_node**       	| **msg_id**        	| **tx_pow**         	| **channel**       	| **rx_robot** 	| **success**              	| **x**                       	| **y**                       	| **theta**     	| **power**                 	| **voltage**     	| **current**     	|
	|-----------------	|------------------------	|-------------------	|-------------------	|--------------------	|-------------------	|--------------	|--------------------------	|-----------------------------	|-----------------------------	|---------------	|---------------------------	|-----------------	|-----------------	|
	| **Description** 	| timestamp of the event 	| id of the tx node 	| id of the message 	| transmitting power 	| frequency channel 	| dest robot   	| message received or not? 	| robot location x coordinate 	| robot location y coordinate 	| robot heading 	| tx node power consumption 	| tx node voltage 	| tx node current 	|
	| **Type**        	| floating               	| string            	| integer           	| integer            	| integer           	| string       	| boolean                  	| float                       	| float                       	| float         	| float                     	| float           	| float           	|
	| **Unit**        	| s                      	| -                 	| -                 	| dBm                	| -                 	| -            	| -                        	| m                           	| m                           	| rad           	| W                         	| V               	| A               	|

- Feature reported on the rx side are:

    |                 	| **timestamp**          	| **rx_node**       	| **robot**            	| **msg_id**        	| **tx_node**       	| **delay**                               	| **rssi**               	| **channel**       	| **x**                       	| **y**                       	| **theta**     	| **power**                 	| **voltage**     	| **current**     	|
    |-----------------	|------------------------	|-------------------	|----------------------	|-------------------	|-------------------	|-----------------------------------------	|------------------------	|-------------------	|-----------------------------	|-----------------------------	|---------------	|---------------------------	|-----------------	|-----------------	|
    | **Description** 	| timestamp of the event 	| id of the rx node 	| name of the rx robot 	| id of the message 	| id of the tx node 	| reception_timestamp - sending_timestamp 	| signal reception power 	| frequency channel 	| robot location x coordinate 	| robot location y coordinate 	| robot heading 	| tx node power consumption 	| tx node voltage 	| tx node current 	|
    | **Type**        	| floating               	| string            	| string               	| integer           	| string            	| float                                   	| integer                	| integer           	| float                       	| float                       	| float         	| float                     	| float           	| float           	|
    | **Unit**        	| s                      	| -                 	| -                    	| -                 	| -                 	| s                                       	| dBm                    	| -                 	| m                           	| m                           	| rad           	| W                         	| V               	| A               	|


## Sample Experiments
As of August 1st, 2022, this repository contains the traces of 20 sample experiments.
They correspond to an Edge Computing use case composed of 100 static Tx nodes that want to offload their data.
Each node belongs to 1 of 3 different applications: VoIP, Surveillance and Emergency Response.
Each application produces data according to 1 of the 3 following modes: periodic (Tx nodes produce data every $x$ milliseconds), event based (modeled with an exponential law with occurrence rate lambda) and hybrid (combination of the two previous modes).

The 20 experiments correspond to varying different parameters of the applications, namely the communication channels, the mobility circuits of the robots and the transmission power of the nodes.
Each combination of parameter is repeated at least twice, while keeping constant the instrinsic parameters of each application (see table below for these parameters).

| Application        	| # of Nodes 	| Generation type 	| Lambda 	| Period (s) 	|
|--------------------	|------------	|-----------------	|--------	|------------	|
| VoIP               	| 50         	| Periodic        	| -      	| 0.063532   	|
| Surveillance       	| 25         	| Exponential     	| 196.74 	| -          	|
| Emergency Response 	| 25         	| Hybrid          	| 0.0333 	| 30         	|
