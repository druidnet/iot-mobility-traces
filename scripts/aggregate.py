import socket
import select
import time
import _thread
from datetime import datetime

hosts = {
    'm3-101': '172.16.74.101',
    'm3-102': '172.16.74.102',
    'm3-103': '172.16.74.103',
    'm3-104': '172.16.74.104',
}
sockets_to_hostnames = {}
sockets_to_files = {}
port = 20000
connecting = True

def connector():
    all_connected = False
    while not all_connected:
        for (hostname, addr) in hosts.items():
            sock = socket.socket()
            if hostname in sockets_to_hostnames.values():
                continue

            # print(f"Connecting to {hostname} ({addr}:{port})")
            try:
                sock.connect((addr, port))
                sockets_to_hostnames[sock] = hostname
                sockets_to_files[sock] = sock.makefile()
                print(f"Connected to {hostname} ({addr}:{port})", flush=True)
            except socket.error as e:
                # print(f"Error while connecting to host {hostname} ({addr}:{port})", flush=True)
                # raise e
                all_connected = False
                time.sleep(0.1)

    time.sleep(0.1)
    connecting = False


def main():
    try:
       _thread.start_new_thread(connector, ())
    except:
       print("Error: unable to start connector thread")
       raise

    sockets = sockets_to_hostnames.keys()
    while True:
        if connecting:
            sockets = sockets_to_hostnames.keys()
        
        ready_sockets,_,_ = select.select(sockets, [], [], 0.1)
        for sock in ready_sockets:
            line = sockets_to_files[sock].readline()
            if line:
                unixtime = datetime.timestamp(datetime.now())
                print(f"{unixtime:.6f};{sockets_to_hostnames[sock]};{line}", end='', flush=True)


if __name__ == '__main__':
    main()