#!/bin/bash
# exp.sh: launch experiment on IoT-lab, log & retrieve results from server

set -e
source ./cfg.sh

#---------------------- TEST ARGUMENTS ----------------------#
if [ "$#" -lt 2 ]; then
	echo "Usage: $0 <exp duration (m)> <exp name> [<exp description>] [<robots_circuit>]"
	exit
fi


exp_duration=$1
exp_name=$2
exp_description=$3
#---------------------- TEST ARGUMENTS ----------------------#


#--------------------- DEFINE VARIABLES ---------------------#
TX_POWER=\"0dBm\"
channels=(11 11 11)
ROBOT_CIRCUIT=$EXPDIR/res/null.json

if [ "$#" -gt 3 ]; then
	TX_POWER=$4
fi
if [ "$#" -gt 4 ]; then
	IFS=', ' read -r -a channels <<< "$5"
fi
if [ "$#" -gt 5 ]; then
	ROBOT_CIRCUIT=$EXPDIR/res/$6
fi
#--------------------- DEFINE VARIABLES ---------------------#


#-------------------- APPLICATIONS CONFIG -------------------#
# Application configs in the format:
# <name> <send_interval_ms> <nb_packets> <payload_size> <lambda> <channel> <power> <nodes> <dest> <robot_ip> <robot_circuit>
# app1=("voip" 			63 		1 	127 	 0 		${channels[0]} 	$TX_POWER 	lille,m3,130-132+134-180  	0xA287 	172.16.74.101	$ROBOT_CIRCUIT)
app1=("voip" 			63 		1 	127 	 0 		${channels[0]} 	$TX_POWER 	lille,m3,130-132+134-180  	0x9688 	172.16.74.102	$ROBOT_CIRCUIT)
# app2=("surveillance"	-1  	1 	127 	 196.74	${channels[1]} 	$TX_POWER 	lille,m3,181-205			0x2656 	172.16.74.103 	$ROBOT_CIRCUIT)
app2=("surveillance"	-1  	1 	127 	 196.74	${channels[1]} 	$TX_POWER 	lille,m3,181-205			0xA287 	172.16.74.101 	$ROBOT_CIRCUIT)
app3=("emergency" 		30000 	1 	127		 0.0333	${channels[2]} 	$TX_POWER 	lille,m3,206-230			0x9987 	172.16.74.104	$ROBOT_CIRCUIT)
all_nodes_list="lille,m3,130-132+134-230"


apps=(
  app1[@]
  app2[@]
  app3[@]
)
NB_APPS=${#apps[@]}
#-------------------- APPLICATIONS CONFIG -------------------#


#----------------------- CATCH SIGINT -----------------------#
# For a clean exit from the experiment
trap ctrl_c INT
function ctrl_c() {
	echo "Terminating experiment."
	iotlab-experiment stop -i "$EXPID" || true
	stop_robots_and_logging
	exit 1
}

stop_robots_and_logging(){
	# Stop the logging process (aggregate.py)
	pkill -SIGTERM -f aggregate.py || true

	echo
	echo "---------------------------------"
	echo "Stopping experiment on robots"
	echo "---------------------------------"
	echo "---------------------------------"
	for (( i=0; i<$NB_APPS; i++ ))
	do
		robot_ip=${!apps[i]:9:1}
		app_name=${!apps[i]:0:1}

		# Stop in background
		{
			# Stop robot
			curl -X DELETE http://$robot_ip:8081/exp/stop; echo
			
			# Stop node
		    curl -X DELETE http://$robot_ip:8080/exp/stop; echo

			# Restart iotlab-ros
			sleep 120 && ssh turtlebot@$robot_ip -p 2222 "sudo service iotlab-ros restart > /dev/null"
		} &
	done
}

write_exp_config(){
	out_file=$1
	
	echo "experiment:" > $out_file
	echo "  id: $EXPID" >> $out_file || true
	echo "  name: $exp_name" >> $out_file
	echo "  duration_mn: $exp_duration" >> $out_file
	echo "  description: $exp_description" >> $out_file
	echo "  nodes: $all_nodes_list" >> $out_file
	echo "  nb_apps: $NB_APPS" >> $out_file
	echo "  apps:" >> $out_file

	for (( i=0; i<$NB_APPS; i++ ))
	do
		app_name=${!apps[i]:0:1}
		send_interval=${!apps[i]:1:1}
		nb_packets=${!apps[i]:2:1}
		paylod_size=${!apps[i]:3:1}
		lambda=${!apps[i]:4:1}
		channel=${!apps[i]:5:1}
		tx_power=${!apps[i]:6:1}
		nodes=${!apps[i]:7:1}
		dest_addr=${!apps[i]:8:1}
		robot_ip=${!apps[i]:9:1}
		robot_circuit=${!apps[i]:10:1}

		robot_circuit=$(tr -d '\n ' < $robot_circuit) || true

		echo "    - app$(($i+1)):" >> $out_file
		echo "        name: $app_name" >> $out_file
		echo "        period: $send_interval" >> $out_file
		echo "        nb_packets_per_period: $nb_packets" >> $out_file
		echo "        paylod_size: $paylod_size" >> $out_file
		echo "        lambda: $lambda" >> $out_file
		echo "        channel: $channel" >> $out_file
		echo "        tx_power: $tx_power" >> $out_file
		echo "        nodes: $nodes" >> $out_file
		echo "        dest_addr: $dest_addr" >> $out_file
		echo "        robot_ip: $robot_ip" >> $out_file
		echo "        robot_circuit: $robot_circuit" >> $out_file
	done

	echo "" >> $out_file
}
#----------------------- CATCH SIGINT -----------------------#



echo
echo "---------------------------------"
echo "Launching experiment on IOT-LAB"
echo "---------------------------------"
echo "---------------------------------"

# Launch the experiment and obtain its ID
exp_dur=$(($exp_duration+5)) # Add 5 minutes security
# exp_dur=$exp_duration
EXPID=$(iotlab-experiment submit -n $exp_name -d $exp_dur -l $all_nodes_list --site-association $SITE,script=serial_script.sh | grep id | cut -d' ' -f6)
echo "Experiment ID: $EXPID"
iotlab-experiment wait -i $EXPID

# Security time (wait that previous experiment --if any-- finishes properly)
sleep 180

#-------------------- BUILD FIRMWARES --------------------#
CODEDIR="${OPENLAB_DIR}/appli/iotlab_examples/tutorial"

cp $CODEDIR/main.c $CODEDIR/main.c.bkp
mkdir -p $EXPDIR/scripts/.tmp

for (( i=0; i<$NB_APPS; i++ ))
do
	app_name=${!apps[i]:0:1}
	send_interval=${!apps[i]:1:1}
	nb_packets=${!apps[i]:2:1}
	paylod_size=${!apps[i]:3:1}
	lambda=${!apps[i]:4:1}
	channel=${!apps[i]:5:1}
	tx_power=${!apps[i]:6:1}
	dest_addr=${!apps[i]:8:1}
	
	echo
	echo "---------------------------------"
	echo "Building application: $app_name"
	echo "---------------------------------"
	echo "---------------------------------"

	# Set app parameters accordingly
	cp $EXPDIR/src/main.c $CODEDIR
	sed -i "s/#define\ CHANNEL\ .*/#define\ CHANNEL\ $channel/g" $CODEDIR/main.c
	sed -i "s/#define\ TX_POWER\ .*/#define\ TX_POWER\ $tx_power/g" $CODEDIR/main.c
	sed -i "s/#define\ DEST_ADDR\ .*/#define\ DEST_ADDR\ $dest_addr/g" $CODEDIR/main.c
	sed -i "s/#define\ IS_RX\ .*/#define\ IS_RX\ 0/g" $CODEDIR/main.c
	sed -i "s/#define\ IS_TX\ .*/#define\ IS_TX\ 1/g" $CODEDIR/main.c
	sed -i "s/#define\ NB_PACKETS\ .*/#define\ NB_PACKETS\ $nb_packets/g" $CODEDIR/main.c
	sed -i "s/#define\ PACKET_LEN\ .*/#define\ PACKET_LEN\ $paylod_size/g" $CODEDIR/main.c
	sed -i "s/#define\ TX_PERIOD_MS\ .*/#define\ TX_PERIOD_MS\ $send_interval/g" $CODEDIR/main.c
	sed -i "s/#define\ LAMBDA\ .*/#define\ LAMBDA\ $lambda/g" $CODEDIR/main.c

	cd $OPENLAB_DIR
	mkdir -p build.m3 && cd build.m3 &&	cmake .. -DPLATFORM=iotlab-m3
	make tutorial_m3 -j8 || { echo "Compilation failed."; exit 1; }
	cp $OPENLAB_DIR/build.m3/bin/tutorial_m3.elf $EXPDIR/scripts/.tmp/sender.$app_name


	# Build receiver (just change from TX to RX)
	sed -i "s/#define\ IS_RX\ .*/#define\ IS_RX\ 1/g" $CODEDIR/main.c
	sed -i "s/#define\ IS_TX\ .*/#define\ IS_TX\ 0/g" $CODEDIR/main.c

	cd $OPENLAB_DIR
	mkdir -p build.m3 && cd build.m3 &&	cmake .. -DPLATFORM=iotlab-m3
	make tutorial_m3 -j8 || { echo "Compilation failed."; exit 1; }
	cp $OPENLAB_DIR/build.m3/bin/tutorial_m3.elf $EXPDIR/scripts/.tmp/receiver.$app_name
done
# -------------------- BUILD FIRMWARES --------------------#


#-------------------- LAUNCH EXPERIMENTS --------------------#
cd $EXPDIR/scripts

# Start local serial aggregator
mkdir -p $EXPDIR/exps/$EXPID/raw/rx/
python $EXPDIR/scripts/aggregate.py > $EXPDIR/exps/$EXPID/raw/rx/rx.log &

# Write experiment configuration
write_exp_config $EXPDIR/exps/$EXPID/exp_desc.yaml

# Force ntp sync
# sudo service ntp stop && sudo ntpd -gq && sudo service ntp start &

echo
echo "---------------------------------"
echo "Launching experiment on robots"
echo "---------------------------------"
echo "---------------------------------"
# Launch robot mobility
for (( i=0; i<$NB_APPS; i++ ))
do
	app_name=${!apps[i]:0:1}
	robot_ip=${!apps[i]:9:1}
	robot_circuit=${!apps[i]:10:1}
	firmware=$EXPDIR/scripts/.tmp/receiver.$app_name

	# Launch robot
	# Robot mobility is controlled via port 8081
	echo "Launching robot mobility for app $app_name"
	robot_cfg=$(python $EXPDIR/scripts/exp_start_cfg.py $robot_circuit	0  ${SSH_KEY})
	robot_cfg=$(echo $robot_cfg)
	curl -X POST -H "Content-Type: application/json" --data "${robot_cfg}" \
    http://$robot_ip:8081/exp/start/$EXPID/$LOGIN; echo

	# Launch node
	# IoTLAB Gateway is controlled via port 8080
	echo "Launching robot node for app $app_name"
	curl -X POST -H "Content-Type: multipart/form-data" http://$robot_ip:8080/exp/start/$EXPID/$LOGIN \
    -F "firmware=@$firmware" -F "profile=@$EXPDIR/res/consumption_profile.json"; echo

    sleep 30
done


echo
echo "---------------------------------"
echo "Flashing IOT-LAB nodes"
echo "---------------------------------"
echo "---------------------------------"
# Flash nodes
iotlab-node --profile-load $EXPDIR/res/consumption_profile.json -i $EXPID
for (( i=0; i<$NB_APPS; i++ ))
do
	app_name=${!apps[i]:0:1}
	nodes_list=${!apps[i]:7:1}
	echo "Flashing $EXPDIR/scripts/.tmp/sender.$app_name on nodes $nodes_list"
	iotlab-node --flash $EXPDIR/scripts/.tmp/sender.$app_name -i $EXPID -l $nodes_list
done
# Wait for OS to start
sleep 10

echo
echo "---------------------------------"
echo "Waiting for experiment termination"
echo "---------------------------------"
echo "---------------------------------"
# Wait for experiment termination
iotlab-experiment wait -i $EXPID --state Terminated
#-------------------- ACTUAL EXPERIMENTS --------------------#


#----------------------- RETRIEVE LOG -----------------------#
mkdir -p $EXPDIR/exps/$EXPID/raw/tx/consumption
mkdir -p $EXPDIR/exps/$EXPID/raw/rx/consumption
mkdir -p $EXPDIR/exps/$EXPID/raw/rx/pose

scp $IOTLAB:.iot-lab/$EXPID/tx.log $EXPDIR/exps/$EXPID/raw/tx/tx.log
scp -r $IOTLAB:.iot-lab/$EXPID/consumption $EXPDIR/exps/$EXPID/raw/tx/ && ssh $IOTLAB "rm -r .iot-lab/$EXPID"

for (( i=0; i<$NB_APPS; i++ ))
do
	robot_ip=${!apps[i]:9:1}

	scp -P 2222 -r turtlebot@$robot_ip:/iotlab/users/$LOGIN/.iot-lab/$EXPID/consumption/* \
	 $EXPDIR/exps/$EXPID/raw/rx/consumption || echo "Error while downloading consumption info from $robot_ip"
	scp -P 2222 -r turtlebot@$robot_ip:/iotlab/users/$LOGIN/.iot-lab/$EXPID/robot/* \
	 $EXPDIR/exps/$EXPID/raw/rx/pose || echo "Error while downloading pose info from $robot_ip"
done
#----------------------- RETRIEVE LOG -----------------------#

# Parse results
python $EXPDIR/scripts/parse_results.py $EXPDIR/exps/$EXPID > /dev/null &

# Stop experiments on robots
stop_robots_and_logging 

# Add this experiment to list of successfull ones
echo "  $exp_name: $EXPID" >> $EXPDIR/exps/experiment_ids.yaml

exit 0
