from os import listdir
from os.path import isfile, join
import os, sys, subprocess, pathlib, argparse

addr_identifier = "Current node uid:"
addr_prefix = ''
tab_sep = ';'
oml_tab_sep = '\t'


addr_to_node = {
	f'{addr_prefix}A287': 'm3-101',
	f'{addr_prefix}9688': 'm3-102',
	f'{addr_prefix}2656': 'm3-103',
	f'{addr_prefix}9987': 'm3-104',
}
node_to_addr = {}

robot_to_node = {
	'robot1': 'm3-101',
	'robot2': 'm3-102',
	'robot3': 'm3-103',
	'robot4': 'm3-104',
}
node_to_robot = {}

robots_docks = {
	'robot1': {
		'x': 19.1,
		'y': 0.329,
		'theta': 4.5,
	},
	'robot2': {
		'x': 20.22,
		'y': 0.329,
		'theta': 4.5,
	},
	'robot3': {
		'x': 21.19,
		'y': 0.329,
		'theta': 4.5,
	},
	'robot4': {
		'x': 23.69,
		'y': 0.329,
		'theta': 4.5,
	},
}

# Handle all open files
open_files = {}



# A hierarchical dict data structure for rx messages
# rx_node -> tx_node -> msg_id > Infos from messages
rx_msgs_index = {}

# List of rx messages in their arriving order
rx_msgs_list = []


# A hierarchical dict data structure for rx messages
# tx_node -> rx_node -> msg_id > Infos from messages
tx_msgs_index = {}
# List of rx messages in their arriving order
tx_msgs_list = []


def main():
	global addr_to_node

	tx_addrs = find_tx_addresses()
	tx_addrs.update(addr_to_node)
	addr_to_node = tx_addrs

	# Fill in the node_to_addr dict
	for key, value in addr_to_node.items():
		node_to_addr[value] = key

	# Fill in the node_to_robot dict
	for key, value in robot_to_node.items():
		node_to_robot[value] = key


	# Index tx and rx files
	print(f'Building tx and rx indices for exp: {exp_dir}')
	build_tx_index()
	build_rx_index()

	# Build datasets on tx and rx sides
	print(f'Building tx dataset for exp: {exp_dir}')
	build_tx_dataset(save=save)
	
	print(f'Building rx dataset for exp: {exp_dir}')
	build_rx_dataset(save=save)


	# Close all open files
	for f in open_files.values():
		f.close()


def build_tx_index():
	global tx_msgs_index
	global tx_msgs_list

	tx_logfile = open_file(tx_log_filename, save=False)
	while True:
		tx_logline = tx_logfile.readline()
		if not tx_logline:
			break

		if 'Sending' in tx_logline:
			cols = tx_logline.split(tab_sep)	
			if len(cols)!= 7:
				continue

			timestamp = cols[0]
			tx_node = cols[1]
			msg_id = cols[3].split('=')[-1]
			rx_addr = cols[4].split('=')[-1]
			tx_pow = cols[5].split('=')[-1].replace('dBm', '')
			channel = cols[6].split('=')[-1].replace('\n', '')
			rx_node = addr_to_node[rx_addr]
			robot = node_to_robot[rx_node]


			tx_node_idx = tx_msgs_index.get(tx_node, {})
			rx_node_idx = tx_node_idx.get(rx_node, {})
			msg_idx = rx_node_idx.get(msg_id, {})
			
			msg_idx['timestamp'] = timestamp
			msg_idx['tx_node'] = tx_node
			msg_idx['msg_id'] = msg_id
			msg_idx['tx_pow'] = tx_pow
			msg_idx['channel'] = channel
			msg_idx['rx_addr'] = rx_addr
			msg_idx['rx_node'] = rx_node
			msg_idx['robot'] = robot

			rx_node_idx[msg_id] = msg_idx
			tx_node_idx[rx_node] = rx_node_idx
			tx_msgs_index[tx_node] = tx_node_idx
			tx_msgs_list.append(msg_idx)

	tx_logfile.close()


def build_rx_index():
	global rx_msgs_index
	global rx_msgs_list

	rx_logfile = open_file(rx_log_filename, save=False)
	while True:
		rx_logline = rx_logfile.readline()
		if not rx_logline:
			break

		if 'Reception' in rx_logline:
			cols = rx_logline.split(tab_sep)
			if len(cols)!= 8:
				continue

			timestamp = cols[0]
			rx_node = cols[1]
			msg_id = cols[3].split('=')[-1]
			datalen = cols[4].split('=')[-1]
			rssi = cols[5].split('=')[-1].replace('dBm', '')
			tx_addr = cols[6].split('=')[-1]
			channel = cols[7].split('=')[-1].replace('\n', '')
			try:
				tx_node = addr_to_node[tx_addr]
			except:
				tx_node = 'Unknown'


			rx_node_idx = rx_msgs_index.get(rx_node, {})
			tx_node_idx = rx_node_idx.get(tx_node, {})
			msg_idx = tx_node_idx.get(msg_id, {})
			
			msg_idx['timestamp'] = timestamp
			msg_idx['rx_node'] = rx_node
			msg_idx['msg_id'] = msg_id
			msg_idx['datalen'] = datalen
			msg_idx['rssi'] = rssi
			msg_idx['tx_addr'] = tx_addr
			msg_idx['tx_node'] = tx_node
			msg_idx['channel'] = channel

			tx_node_idx[msg_id] = msg_idx
			rx_node_idx[tx_node] = tx_node_idx
			rx_msgs_index[rx_node] = rx_node_idx
			rx_msgs_list.append(msg_idx)


	rx_logfile.close()


def build_tx_dataset(save=True):
	# Write the output to file, while also printing it to the console
	outfile = sys.stdout
	if save:
		log_dir = f'{exp_dir}/aggr'
		log_file = f'{log_dir}/tx_aggr.log'
		pathlib.Path(log_dir).mkdir(parents=True, exist_ok=True)
		outfile = open(log_file, 'w')

		# tee = subprocess.Popen(["tee", log_file], stdin=subprocess.PIPE)
		# os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
		# os.dup2(tee.stdin.fileno(), sys.stderr.fileno())


	print(f'timestamp(s){tab_sep}tx_node{tab_sep}msg_id{tab_sep}tx_pow(dBm){tab_sep}channel{tab_sep}rx_robot{tab_sep}success', end='', file=outfile)
	print(f'{tab_sep}x(m){tab_sep}y(m){tab_sep}theta(rad){tab_sep}power(W){tab_sep}voltage(V){tab_sep}current(A)', file=outfile)

	nb_msgs = 0
	for msg in tx_msgs_list:
		timestamp = msg['timestamp']
		tx_node = msg['tx_node']
		msg_id = msg['msg_id']
		tx_pow = msg['tx_pow']
		channel = msg['channel']
		rx_addr = msg['rx_addr']
		rx_node = msg['rx_node']
		robot = msg['robot']

		# nb_msgs += 1
		# if nb_msgs >= 100:
		# 	break

		# print('------------------- was_packet_received', flush=True)
		try:
			if rx_msgs_index[rx_node][tx_node][msg_id]['msg_id'] == msg_id:
				packet_received = 1
		except:
			packet_received = 0

		# print('------------------- find_robot_pose', flush=True)
		pose = find_robot_pose(robot=rx_node, timestamp=float(timestamp))
		if pose is None:
			pose = robots_docks.get(robot, {'x': None, 'y': None, 'theta': None})
		x = pose['x']
		y = pose['y']
		theta = pose['theta']

		power, voltage, current = (None, None, None)
		# print('------------------- find_node_consumption', flush=True)
		consumption = find_node_consumption(node=tx_node.replace('-', '_'), timestamp=float(timestamp))
		if consumption is not None:
			power = consumption['power']
			voltage = consumption['voltage']
			current = consumption['current']

		print(f'{timestamp}{tab_sep}{tx_node}{tab_sep}{msg_id}{tab_sep}{tx_pow}{tab_sep}{channel}{tab_sep}{robot}{tab_sep}{packet_received}', end='', file=outfile)
		print(f'{tab_sep}{x}{tab_sep}{y}{tab_sep}{theta}{tab_sep}{power}{tab_sep}{voltage}{tab_sep}{current}', file=outfile)


	# Terminate tee process
	if save:
		# tee.terminate()
		outfile.close



def build_rx_dataset(save=True):
	# Write the output to file, while also printing it to the console
	outfile = sys.stdout
	if save:
		log_dir = f'{exp_dir}/aggr'
		log_file = f'{log_dir}/rx_aggr.log'
		pathlib.Path(log_dir).mkdir(parents=True, exist_ok=True)
		outfile = open(log_file, 'w')

		# tee = subprocess.Popen(["tee", log_file], stdin=subprocess.PIPE)
		# os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
		# os.dup2(tee.stdin.fileno(), sys.stderr.fileno())

		
	print(f'timestamp(s){tab_sep}rx_node{tab_sep}robot{tab_sep}msg_id{tab_sep}tx_node{tab_sep}delay(s){tab_sep}rssi(dBm){tab_sep}channel', end='', file=outfile)
	print(f'{tab_sep}x(m){tab_sep}y(m){tab_sep}theta(rad){tab_sep}power(W){tab_sep}voltage(V){tab_sep}current(A)', file=outfile)

	nb_msgs = 0
	for msg in rx_msgs_list:
		timestamp = msg['timestamp']
		rx_node = msg['rx_node']
		msg_id = msg['msg_id']
		datalen = msg['datalen']
		rssi = msg['rssi']
		channel = msg['channel']
		tx_addr = msg['tx_addr']
		tx_node = msg['tx_node']
		robot = node_to_robot[rx_node]

		# nb_msgs += 1
		# if nb_msgs >= 100:
		# 	break

		# timestamp_sent = sending_timestamp(tx_node, msg_id, node_to_addr[rx_node])
		try:
			timestamp_sent = tx_msgs_index[tx_node][rx_node][msg_id]['timestamp']
			delay = float(timestamp) - float(timestamp_sent)
			delay = max(delay, 0)
		except:
			delay = -1
			# raise
		
		pose = find_robot_pose(robot=rx_node, timestamp=float(timestamp))
		if pose is None:
			pose = robots_docks.get(robot, {'x': None, 'y': None, 'theta': None})
		
		x = pose['x']
		y = pose['y']
		theta = pose['theta']

		power, voltage, current = (None, None, None)
		consumption = find_node_consumption(node=rx_node.replace('-', '_'), timestamp=float(timestamp), tx=False)
		if consumption is not None:
			power = consumption['power']
			voltage = consumption['voltage']
			current = consumption['current']

		print(f'{timestamp}{tab_sep}{rx_node}{tab_sep}{robot}{tab_sep}{msg_id}{tab_sep}{tx_node}{tab_sep}{delay:.6f}{tab_sep}{rssi}{tab_sep}{channel}', end='', file=outfile)
		print(f'{tab_sep}{x}{tab_sep}{y}{tab_sep}{theta}{tab_sep}{power}{tab_sep}{voltage}{tab_sep}{current}', file=outfile)


	# Terminate tee process
	if save:
		# tee.terminate()
		outfile.close()


def find_tx_addresses():
	addrs = {}

	# Init with node properties
	try:
		csvfile = open_file(node_properties_file)
		lines = csvfile.readlines()
		for line in lines[1:]:
			cols = line.split(',')
			node_name = cols[0].split('.')[0]
			node_uid = cols[3].upper()
			addrs[node_uid] = node_name

	except:
		print(f'Unable to load node properties file: {node_properties_file}')
		raise


	# Update with uid from tx log file
	tx_nodes = [f.replace('.oml', '').replace('_', '-') for f in listdir(tx_consumption_dir)
				if isfile(join(tx_consumption_dir, f))]

	for node in tx_nodes:
		with open(tx_log_filename, 'r') as f:
			while True:
				line = f.readline()
				if not line:
					break

				# Select the first line having the information
				if (node in line) and (addr_identifier in line):
					uid = line.split(addr_identifier)[-1].strip()
					addr = addr_prefix + uid.replace('\n', '')

					# Save the identified address
					addrs[addr] = node
					break

	return addrs

def was_packet_received(tx_node_addr, msg_id, rx_node):
	exprs_to_find = (f'{tab_sep}{rx_node}{tab_sep}Reception{tab_sep}MsgId={msg_id}{tab_sep}', f'{tab_sep}From={tx_node_addr}')
	line = first_line_containing(rx_log_filename, exprs_to_find)

	# print("###############", exprs_to_find)

	return bool(line)

def sending_timestamp(tx_node, msg_id, rx_node_addr):
	exprs_to_find = (f'{tab_sep}{tx_node}{tab_sep}Sending{tab_sep}MsgId={msg_id}{tab_sep}To={rx_node_addr}{tab_sep}')
	line = first_line_containing(tx_log_filename, exprs_to_find)

	if not line:
		return None

	timestamp = line.split(tab_sep)[0]
	return float(timestamp)

def find_robot_pose(robot, timestamp):
	if not hasattr(find_robot_pose, 'robot_pose_idx'):
		find_robot_pose.robot_pose_idx = {}

	robot_pose_filename = f'{rx_dir}/pose/{robot}.oml'
	if robot_pose_filename not in find_robot_pose.robot_pose_idx:
		# Build index for this file
		try:
			poses = []
			file = open_file(robot_pose_filename, save=False)
			lines = file.readlines()
			for line in lines:
				cells = line.split(oml_tab_sep)
				if len(cells) != 8:
					continue

				try:
					current_timestamp = float(f'{cells[3]}.{cells[4]}')
					x = cells[5]
					y = cells[6]
					theta = cells[7].replace('\n', '')

					pose = {}
					pose['timestamp'] = current_timestamp
					pose['x'] = x
					pose['y'] = y
					pose['theta'] = theta
					poses.append(pose)
				except:
					continue

			find_robot_pose.robot_pose_idx[robot_pose_filename] = poses
		except:
			return None


	robot_poses = find_robot_pose.robot_pose_idx[robot_pose_filename]
	for pose in robot_poses:
		if pose['timestamp'] >= timestamp:
			return pose


def find_node_consumption(node, timestamp, tx=True):
	if not hasattr(find_node_consumption, 'node_consumptions_idx'):
		find_node_consumption.node_consumptions_idx = {}
	
	base_dir = tx_dir
	if not tx:
		base_dir = rx_dir

	node_consumption_filename = f'{base_dir}/consumption/{node}.oml'
	if node_consumption_filename not in find_node_consumption.node_consumptions_idx:
		# Build index for this file
		try:
			consumptions = []
			file = open_file(node_consumption_filename, save=False)
			lines = file.readlines()
			for line in lines:
				cells = line.split(oml_tab_sep)
				if len(cells) != 8:
					continue

				try:
					current_timestamp = float(f'{cells[3]}.{cells[4]}')
					power = cells[5]
					voltage = cells[6]
					current = cells[7].replace('\n', '')

					cons = {}
					cons['timestamp'] = current_timestamp
					cons['power'] = power
					cons['voltage'] = voltage
					cons['current'] = current
					consumptions.append(cons)
				except:
					continue

			find_node_consumption.node_consumptions_idx[node_consumption_filename] = consumptions
		except:
			raise


	node_consumptions = find_node_consumption.node_consumptions_idx[node_consumption_filename]
	for cons in node_consumptions:
		if cons['timestamp'] >= timestamp:
			return cons


def first_line_containing(filename, exprs):
	file = open_file(filename)
	file_pos = []
	n_lines = 0
	while True:
		file_pos.append(file.tell())
		line = file.readline()
		if not line:
			break

		# print("####################", n_lines)
		n_lines += 1
		if n_lines > 500:
			break

		if (exprs[0] in line) and ('Debugging soft timer' in line):
			# The node has crashed
			break 

		found = True
		for expr in exprs:
			if expr not in line:
				found = False
				break

		if found:
			pos_100_lines_before = file_pos[max(0, len(file_pos) - 100)]
			file.seek(pos_100_lines_before)
			return line
		
	file.seek(file_pos[0])
	return None

def first_row_after_timestamp(filename, timestamp):
	try:
		file = open_file(filename)
		file_pos = []
		while True:
			file_pos.append(file.tell())
			line = file.readline()
			if not line:
				break

			line = line.replace('\n', '')
			cells = line.split(oml_tab_sep)
			if len(cells) != 8:
				continue

			try:
				current_timestamp = float(f'{cells[3]}.{cells[4]}')
			except:
				continue

			# This is the line we were looking for (first line after the input timestamp)
			if current_timestamp >= timestamp:
				pos_100_lines_before = file_pos[max(0, len(file_pos) - 100)]
				file.seek(pos_100_lines_before)
				return cells
	except:
		pass
	
	file.seek(file_pos[0])
	return None




def open_file(filename, save=False):
	# return open(filename, 'r')

	if not save:
		return open(filename, 'r')

	if filename not in open_files:
		open_files[filename] = open(filename, 'r')

	return open_files[filename]

def close_file(filename):
	if filename in open_files:
		open_files[filename].close()


if __name__ == '__main__':
	def str2bool(v):
		if isinstance(v, bool):
			return v
		if v.lower() in ('yes', 'true', 't', 'y', '1'):
			return True
		elif v.lower() in ('no', 'false', 'f', 'n', '0'):
			return False
		else:
			raise argparse.ArgumentTypeError('Boolean value expected.')

	# ------------- Parse arguments ---------------
	save = True
	node_properties_file = '../node-properties.csv'

	parser = argparse.ArgumentParser(description='Parse experiment logs and produce well formated datasets')
	parser.add_argument('exp_dir', help=f'Experiment dir.\n')
	parser.add_argument('-np', '--node_properties_file', default=node_properties_file, help=f'Node properties file.\nDefault: {node_properties_file}')
	parser.add_argument('-s', '--save', default=True, type=str2bool, help=f'Save resulting datasets to files.\nDefault: {save}')
	args = parser.parse_args()

	exp_dir = args.exp_dir
	node_properties_file = args.node_properties_file
	save = args.save
	# ------------- Parse arguments ---------------
	
	tx_dir = f'{exp_dir}/raw/tx'
	rx_dir = f'{exp_dir}/raw/rx'
	rx_log_filename = f'{rx_dir}/rx.log'
	tx_log_filename = f'{tx_dir}/tx.log'
	tx_consumption_dir = f'{tx_dir}/consumption'

	main()