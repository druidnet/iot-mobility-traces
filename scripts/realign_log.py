#!/bin/python3

"""
This script helps to 'realign data from experiments log'

There were two mistakes in the source code src/main.c (up to July 8th 2022)
- 1st mistake: msg_ids in tx logs were all incremented by 1
- 2nd mistake: msg_ids in rx logs were printed as uint8 (from 0 to 255), meaning that they always restarted at 0 after 255

This file helps to correct these mistakes.
These mistakes are now solved.
"""

from os import listdir
from os.path import isfile, join
import os, subprocess, pathlib, argparse


def main():
	if receiver:
		realign_rx_logfile()
	else:
		# Realign tx log file
		realign_tx_logfile()

	

def realign_tx_logfile():
	with open(logfilename, 'r') as logfile:
		min_msg_id = 1000
		lines = logfile.readlines()
		for line in lines:
			if not 'Sending' in line:
				print(line, end='')
				continue

			cols = line.split(';')
			if len(cols) != 6:
				continue

			msg_id = int(cols[3].replace('MsgId=', ''))
			if msg_id < min_msg_id:
				min_msg_id = msg_id
			print(f'{cols[0]};{cols[1]};{cols[2]};MsgId={msg_id - min_msg_id};{cols[4]};{cols[5]}', end='')


def realign_rx_logfile():
	history = {}

	with open(logfilename, 'r') as logfile:
		lines = logfile.readlines()
		for line in lines:
			if 'Reception' not in line:
				print(line, end='')
				continue

			cols = line.split(';')
			if len(cols) != 7:
				print(line, end='')
				continue

			rx_node = cols[1]
			msg_id = int(cols[3].replace('MsgId=', ''))
			tx_addr = cols[6].split('=')[-1].replace('\n', '')


			# Check that both rx_node and tx_addr are in the history
			if rx_node not in history:
				history[rx_node] = {}

			if tx_addr not in history[rx_node]:
				history[rx_node][tx_addr] = {'turns': 0, 'msg_id': 0}
			

			# Compute corrected message id
			state = history[rx_node][tx_addr]
			if msg_id - state['msg_id'] < -50:
				state['turns'] = state['turns'] + 1
				# print(rx_node, tx_addr, state)
				# input()
			
			correct_msg_id = state['turns'] * 256 + msg_id
			# correct_msg_id = msg_id % 256
			print(f'{cols[0]};{cols[1]};{cols[2]};MsgId={correct_msg_id};{cols[4]};{cols[5]};{cols[6]}', end='')


			
			state['msg_id'] = msg_id
			history[rx_node][tx_addr] = state



if __name__ == '__main__':
	def str2bool(v):
		if isinstance(v, bool):
			return v
		if v.lower() in ('yes', 'true', 't', 'y', '1'):
			return True
		elif v.lower() in ('no', 'false', 'f', 'n', '0'):
			return False
		else:
			raise argparse.ArgumentTypeError('Boolean value expected.')

	# ------------- Parse arguments ---------------
	parser = argparse.ArgumentParser(description='Realign log file (correct mistake in source code)')
	parser.add_argument('file', help=f'File to realign')
	parser.add_argument('-r', '--rx', type=str2bool, default=False, help=f'Receiver file or not ?')
	args = parser.parse_args()

	logfilename = args.file
	receiver = args.rx
	# ------------- Parse arguments ---------------
	
	main()