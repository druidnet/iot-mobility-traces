#!/bin/bash

LOGIN="jiokeng"
SITE="lille"
IOTLAB="$LOGIN@$SITE.iot-lab.info"
IOTLABDIR="${HOME}/inria/druid-net/iot-lab"
OPENLAB_DIR="${IOTLABDIR}/parts/openlab"
EXPDIR="${HOME}/inria/druid-net/code"
SSH_KEY='~/.ssh/id_rsa.pub'
