#! /usr/bin/env python

import os
import sys
import json


USAGE = 'Usage: %s <mobility_file> <robot_user_env:int> <ssh_key_file>'
USAGE = USAGE % sys.argv[0]


def parse_args():
    try:
        mobility_file = os.path.expanduser(sys.argv[1])
        robot_user_env = bool(int(sys.argv[2]))
        ssh_key_file = os.path.expanduser(sys.argv[3])
    except IndexError:
        print >> sys.stderr, USAGE
        exit(1)

    with open(mobility_file) as _mob:
        mobility = json.load(_mob)
    with open(ssh_key_file) as _key:
        ssh_key = _key.read()

    return mobility, robot_user_env, ssh_key

def main():
    mobility, robot_user_env, ssh_key = parse_args()

    # No 'type' in install-lib json
    if mobility is not None:
        mobility.setdefault('type', 'predefined')

    ret = {
        'mobility': mobility,
        'robot_user_env': robot_user_env,
        'ssh_keys': ['# SSH KEYS\n', ssh_key],
    }

    print(json.dumps(ret, indent=4, sort_keys=True))


if __name__ == '__main__':
    main()
