#!/bin/bash

# This script is a helper one to parse raw logs for all experiments in exps dir
# and create aggregated datasets


source ./cfg.sh

exp_ids=$(ls -d $EXPDIR/exps/*/)

for EXPID in ${exp_ids[@]}
do
	# rm -r $EXPID/aggr/
	
	echo "Parsing results of exp $EXPID"
	python $EXPDIR/scripts/parse_results.py $EXPID &

	echo
 	echo
done
