#!/bin/bash

# This script is a helper one to download remote logs for all experiments in exps dir

source ./cfg.sh

robot_ips=(172.16.74.101 172.16.74.103 172.16.74.103 172.16.74.104)
exp_ids=$(ls -d $EXPDIR/exps/*/)


for EXPID in ${exp_ids[@]}
do
	mkdir -p $EXPDIR/exps/$EXPID/raw/tx/consumption
	mkdir -p $EXPDIR/exps/$EXPID/raw/rx/consumption
	mkdir -p $EXPDIR/exps/$EXPID/raw/rx/pose

	scp $IOTLAB:.iot-lab/$EXPID/serial_output $EXPDIR/exps/$EXPID/raw/tx/tx.log || echo "Error while donwloading remote log files"
	scp -r $IOTLAB:.iot-lab/$EXPID/consumption $EXPDIR/exps/$EXPID/raw/tx/ || echo "Error while donwloading remote log files"


	for robot in ${robot_ips[@]}
	do
		scp -P 2222 -r turtlebot@$robot:/iotlab/users/$LOGIN/.iot-lab/$EXPID/consumption/* $EXPDIR/exps/$EXPID/raw/rx/consumption || echo "Error for $robot/consumption"
		scp -P 2222 -r turtlebot@$robot:/iotlab/users/$LOGIN/.iot-lab/$EXPID/robot/* $EXPDIR/exps/$EXPID/raw/rx/pose || echo "Error for $robot/pose"
	done
done