#!/bin/bash

# This script helps to 'realign data from experiments log'

# There were two mistakes in the source code src/main.c (up to July 8th 2022)
# - 1st mistake: msg_ids in tx logs were all incremented by 1
# - 2nd mistake: msg_ids in rx logs were printed as uint8 (from 0 to 255), meaning that they always restarted at 0 after 255

# This file helps to correct these mistakes.
# These mistakes are now solved.


source ./cfg.sh

exp_ids=$(ls -d $EXPDIR/exps/*/)

for EXPID in ${exp_ids[@]}
do
	echo "Realigning tx log for exp $EXPID"
	# cp $EXPID/raw/tx/tx.log.bkp $EXPID/raw/tx/tx.log
	(cp $EXPID/raw/tx/tx.log $EXPID/raw/tx/tx.log.bkp && \
	python $EXPDIR/scripts/realign_log.py $EXPID/raw/tx/tx.log.bkp > $EXPID/raw/tx/tx.log) || true

	echo "Realigning rx log for exp $EXPID"
	# cp $EXPID/raw/rx/rx.log.bkp $EXPID/raw/rx/rx.log
	(cp $EXPID/raw/rx/rx.log $EXPID/raw/rx/rx.log.bkp && \
	python $EXPDIR/scripts/realign_log.py -r t $EXPID/raw/rx/rx.log.bkp > $EXPID/raw/rx/rx.log) || true

	echo
done