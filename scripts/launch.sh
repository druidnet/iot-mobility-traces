#!/bin/bash


./exp.sh 300 static-5h-same_channels-0dBm-round_1 \
 "Diff channels, tx_power=0dBm, robots static" \"0dBm\" 11,11,11 null.json &

sleep 30
./exp.sh 300 static-5h-same_channels-m12dBm-round_1 \
 "Diff channels, tx_power=-12dBm, robots static" \"-12dBm\" 11,11,11 null.json &

sleep 30
./exp.sh 300 static-5h-same_channels-0dBm-round_2 \
 "Diff channels, tx_power=0dBm, robots static" \"0dBm\" 11,11,11 null.json &

./exp.sh 300 static-5h-same_channels-m12dBm-round_2 \
 "Diff channels, tx_power=-12dBm, robots static" \"-12dBm\" 11,11,11 null.json &

sleep 30
./exp.sh 120 circuit_square-2h-same_channels-0dBm-round_3 \
 "Same channels, tx_power=0dBm, robots performing a large square loop" \"0dBm\" 11,11,11 circuit_square_big.json


