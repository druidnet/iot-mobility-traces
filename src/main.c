#include <platform.h>
#include <stdint.h>
#include <stdlib.h>
#include <printf.h>
#include <string.h>
#include <time.h>

#include "phy.h"
#include "soft_timer.h"
#include "event.h"
#include "random.h"
#include "math.h"

#ifdef IOTLAB_M3
#include "lps331ap.h"
#include "isl29020.h"
#endif
#include "iotlab_uid.h"
#include "mac_csma.h"
#include "phy.h"
#include "iotlab_i2c.h"

#include "iotlab_uid_num_hashtable.h"
#define ADDR_BROADCAST  0xFFFF


/* Configure this parameters */
#define CHANNEL         11       // Communication channel. Choose in [11-26]
#define TX_POWER        "0dBm"   // Transmit power
// Valid  values are "-17dBm" "-12dBm" "-9dBm" "-7dBm" 
// "-5dBm" "-4dBm" "-3dBm" "-2dBm" "-1dBm" "0dBm" "0.7dBm"
// "1.3dBm" "1.8dBm" "2.3dBm" "2.8dBm" "3dBm"

#define DEST_ADDR       0x9989   // Destination address (UID)
#define IS_RX           1        // Should this node receive data
#define IS_TX           1        // Should this node send data
#define NB_PACKETS      1        // Number of packets per period
#define PACKET_LEN      10       // Length of each packet to be sent

#define TX_PERIOD_MS    1000     // Interval between consecutive sent packets (in ms)
#define LAMBDA          0
#define BEGIN_DELAY_MS  10000    // Delay before starting (in ms)
/*End of parameters configuration*/


#if PACKET_LEN > PHY_MAX_TX_LENGTH - 4
#define REAL_PACKET_LEN (PHY_MAX_TX_LENGTH - 4)
#else
#define REAL_PACKET_LEN PACKET_LEN
#endif


// timer alarm function
static void periodic_handler(handler_arg_t arg);
static void exp_handler(handler_arg_t arg);
static void start_handler(handler_arg_t arg);
static soft_timer_t begin_timer;
static soft_timer_t periodic_timer;
static soft_timer_t exp_timer;

#define RANDOM_DELAY            (random_rand32() % (TX_PERIOD_MS))
#define BEGIN_DELAY_MS_TICKS    soft_timer_ms_to_ticks(BEGIN_DELAY_MS + RANDOM_DELAY)
#define TX_PERIOD_TICKS         soft_timer_ms_to_ticks(TX_PERIOD_MS)




/**
 * Tx Power
 * Copied from phy_power.c.h
 */
struct _power_assoc {
    const char *name;
    const phy_power_t power;
};

static const struct _power_assoc _power_dict[] = {
    {"-17dBm", PHY_POWER_m17dBm},
    {"-12dBm", PHY_POWER_m12dBm},
    {"-9dBm", PHY_POWER_m9dBm},
    {"-7dBm", PHY_POWER_m7dBm},
    {"-5dBm", PHY_POWER_m5dBm},
    {"-4dBm", PHY_POWER_m4dBm},
    {"-3dBm", PHY_POWER_m3dBm},
    {"-2dBm", PHY_POWER_m2dBm},
    {"-1dBm", PHY_POWER_m1dBm},
    {"0dBm", PHY_POWER_0dBm},
    {"0.7dBm", PHY_POWER_0_7dBm},
    {"1.3dBm", PHY_POWER_1_3dBm},
    {"1.8dBm", PHY_POWER_1_8dBm},
    {"2.3dBm", PHY_POWER_2_3dBm},
    {"2.8dBm", PHY_POWER_2_8dBm},
    {"3dBm", PHY_POWER_3dBm},
    {NULL, 0},
};

static unsigned int parse_power_rf231(const char *power_str)
{
    /* valid PHY_POWER_ values for rf231 */
    const struct _power_assoc *cur;
    for (cur = _power_dict; cur->name; cur++)
        if (strcmp(cur->name, power_str) == 0)
            return cur->power;
    return 255;
}



/**
 * Node UID
 */
static void print_node_uid()
{
    uint16_t node_uid = iotlab_uid();
    printf("Current node uid: %04x\n", node_uid);
}

static void send_packet()
{
    static uint32_t packet_id = 0;
    static char packet[REAL_PACKET_LEN];
    // rand_string(packet, REAL_PACKET_LEN);

    memcpy(packet, &packet_id, sizeof(packet_id));

    printf("Sending;MsgId=%d;To=%04x;TxPow=%s;Channel=%d\n", packet_id, DEST_ADDR, TX_POWER, CHANNEL);
    mac_csma_data_send(DEST_ADDR, (uint8_t *)packet, REAL_PACKET_LEN);

    packet_id++;

    // Blink every time a packet is sent
    leds_toggle(LED_0 | LED_1 | LED_2);
}


void receive_packet(uint16_t src_addr,
        const uint8_t* data, uint8_t length, int8_t rssi, uint8_t lqi)
{   
    uint32_t msg_id;
    memcpy(&msg_id, data, sizeof(uint32_t));

    printf("Reception;MsgId=%d;Len=%d;Rssi=%ddBm;From=%04x;Channel=%d\n",
        msg_id, length, rssi, src_addr, CHANNEL);

    // Blink every time a packet is received
    leds_toggle(LED_0 | LED_1 | LED_2);

}

/* Reception of a radio message */
void mac_csma_data_received(uint16_t src_addr,
        const uint8_t *data, uint8_t length, int8_t rssi, uint8_t lqi)
{
    if(IS_RX){
        receive_packet(src_addr, data, length, rssi, lqi);
    }
}

double ran_expo(double lambda){
    double u;
    u = rand() / (RAND_MAX + 1.0);
    return -log(1- u) / lambda;
}

void send_traffic(){
    // Send traffic
    int i;
    for(i=0; i<NB_PACKETS; i++){
        send_packet();
    }
}

static void exp_handler(handler_arg_t arg)
{
    // Send traffic
    send_traffic();

    double next_event_time = ran_expo(LAMBDA);
    next_event_time = next_event_time * 1000000; //Convert to microseconds
    
    soft_timer_start(&exp_timer, soft_timer_us_to_ticks((uint32_t) next_event_time), 0);
}

static void periodic_handler(handler_arg_t arg)
{
    // Send traffic
    send_traffic();
}

static void hardware_init()
{
    // Openlab platform init
    platform_init();
    event_init();
    soft_timer_init();

    // Switch off the LEDs
    leds_off(LED_0 | LED_1 | LED_2);

    // Init csma Radio mac layer
    mac_csma_init(CHANNEL, parse_power_rf231(TX_POWER));

    // Init control_node i2c
    iotlab_i2c_init();
}

void start_handler(handler_arg_t arg){
    print_node_uid();

    // Initialize timers
    if(IS_TX){
        if(TX_PERIOD_MS > 0){
            soft_timer_set_handler(&periodic_timer, periodic_handler, NULL);
            soft_timer_start(&periodic_timer, TX_PERIOD_TICKS, 1);
        }

        if(LAMBDA > 0){
            soft_timer_set_handler(&exp_timer, exp_handler, NULL);
            soft_timer_start(&exp_timer, soft_timer_ms_to_ticks(1000), 0);
        }
    }
}

int main()
{
    // Initialization
    hardware_init();

    // Start everything at the specified time
    soft_timer_set_handler(&begin_timer, start_handler, NULL);
    soft_timer_start(&begin_timer, BEGIN_DELAY_MS_TICKS, 0);

    // Start the whole process
    platform_run();

    return 0;
}